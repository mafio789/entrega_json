﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFEntrega1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public List<Persona1> ConsultarPersonas()
        {
            List<Persona1> personas = new List<Persona1>();

            personas.Add(new Persona1() { id = 1, nombre = "Hernan", apellidos = "Pinilla", correo = "t@hotmail.com" });
            personas.Add(new Persona1() { id = 2, nombre = "Jose", apellidos = "Lopez", correo = "p@hotmail.com" });
            personas.Add(new Persona1() { id = 3, nombre = "Jaime", apellidos = "Carrera", correo = "j@hotmail.com" });
            personas.Add(new Persona1() { id = 4, nombre = "Alejandro", apellidos = "Calle", correo = "m@hotmail.com" });
            personas.Add(new Persona1() { id = 5, nombre = "Cesar", apellidos = "Perez", correo = "d@hotmail.com" });

            return personas;
        }
    }
}
