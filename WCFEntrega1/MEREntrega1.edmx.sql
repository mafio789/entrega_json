
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/17/2015 15:43:23
-- Generated from EDMX file: c:\users\chtva\documents\visual studio 2013\Projects\WCFEntrega1\WCFEntrega1\MEREntrega1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BDEntrega1];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'EmpleadoSet'
CREATE TABLE [dbo].[EmpleadoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [apellido] nvarchar(max)  NOT NULL,
    [correo] nvarchar(max)  NOT NULL,
    [Genero_Id] int  NOT NULL
);
GO

-- Creating table 'GeneroSet'
CREATE TABLE [dbo].[GeneroSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre_genero] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'EmpleadoSet'
ALTER TABLE [dbo].[EmpleadoSet]
ADD CONSTRAINT [PK_EmpleadoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GeneroSet'
ALTER TABLE [dbo].[GeneroSet]
ADD CONSTRAINT [PK_GeneroSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Genero_Id] in table 'EmpleadoSet'
ALTER TABLE [dbo].[EmpleadoSet]
ADD CONSTRAINT [FK_GeneroEmpleado]
    FOREIGN KEY ([Genero_Id])
    REFERENCES [dbo].[GeneroSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GeneroEmpleado'
CREATE INDEX [IX_FK_GeneroEmpleado]
ON [dbo].[EmpleadoSet]
    ([Genero_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------