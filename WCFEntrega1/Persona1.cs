﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WCFEntrega1
{
     [DataContract]
    public class Persona1
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public string apellidos { get; set; }

        [DataMember]
        public string correo { get; set; }
    }
}